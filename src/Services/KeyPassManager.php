<?php


namespace App\Services\KeyPass;


use App\Entity\Security\Back\KeyPass;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class KeyPassManager implements KeyPassInterface
{
//    private $createdAt = new \DateTime("now");
//    private $salt = rand(1000, 9999);
    private $security;
    private $em;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->em = $entityManager;
    }

    public function upsert(KeyPass $keyPass): KeyPass
    {
        // TODO: Implement upsert() method.
        //owner
        $keyPass->setOwner($this->security->getUser());
        $salt = rand(10, 99);
        $created_at = new \DateTime('now');
        $keyPass->setSalt($salt);
        $keyPass->setCreatedAt($created_at);
        $pwd = $keyPass->getPassword();
        $pwd = $this->encode_pwd($pwd, $salt, $created_at->getTimestamp());
        $keyPass->setPassword($pwd);

        $this->checkUniqueness($keyPass);

        $this->em->persist($keyPass);
        $this->em->flush();

        return $keyPass;
    }

    public function displayPassword(KeyPass $keyPass): string
    {
        // TODO: Implement displayPassword() method.
        $salt = $keyPass->getSalt();
        $pwd = $keyPass->getPassword();
        $createdAt= $keyPass->getCreatedAt()->getTimestamp();
        $pwd = $this->decode_pwd($pwd, $salt, $createdAt);
        return $pwd;
    }

    private function encode_pwd(string $pwd, int $salt, int $createdAt):string
    {
        $pwd_split = str_split($pwd);
        $result="";
        foreach ($pwd_split as $key=> $value)
        {
            $value = $this->encrypt_code(ord($value), $createdAt);
            $result.=$value.strlen($value).$salt;
            $salt*=4;
        }
        return $result;
    }

    private function decode_pwd(string $pwd_encrypt, int $salt, int $timestamp) : string
    {
        $result = '';

        while (strlen($pwd_encrypt) > 0)
        {
            $char_encoded_with_length = explode($salt, $pwd_encrypt);
            $char_encoded_length = intval(substr($char_encoded_with_length[0], -2, 2));
            $char_encoded = intval(substr($char_encoded_with_length[0], 0, strlen($char_encoded_with_length[0])-strlen($char_encoded_length)));
            $result .= chr($this->decrypt_code($char_encoded, $timestamp));

            // extract crtypedchar with his length
            $pwd_encrypt = substr_replace($pwd_encrypt,'', 0, strlen($char_encoded_with_length[0]));

            // now extract salt
            $pwd_encrypt = substr_replace($pwd_encrypt,'', 0, strlen($salt));
            $salt = $salt * 4;
        }

        return $result;
    }

    private function encrypt_code(int $char_acsii_code, int $timestamp)
    {
        return $char_acsii_code*$timestamp;
    }

    private function decrypt_code (int $char_ascii_code, int $timestamp)
    {
        return $char_ascii_code/$timestamp;
    }

    private function checkUniqueness (KeyPass $keypass){
      $array_ids = [];
      foreach ($keypass->getSharedWith() as $bu){
          if(!in_array($bu->getId(), $array_ids)){
              $array_ids[]=$bu->getId();
          }else{
              $keypass->removeSharedWith($bu);
          }
      }
    }

}