<?php

namespace App\Controller\Back\Settings;

use App\Entity\Security\Back\BackUser;
use App\Entity\Security\Back\KeyPass;
use App\Form\Back\KeyPass\AddKeyPassType;
use App\Form\Back\KeyPass\EditKeyPassType;
use App\Form\Back\KeyPass\ShareKeypass;
use App\Form\KeyPassType;
use App\Repository\Security\Back\KeyPassRepository;
use App\Services\KeyPass\KeyPassInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * Class RecordController
 * @package App\Controller\Back\Settings
 * @Route("/key/pass")
 */
class KeyPassController extends AbstractController
{
    /**
     * @Route("/", name="back_key_pass_index")
     */
    public function listKeyPassActon(Request $request, KeyPassInterface $keyPassManager,
                                     Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addItem("Accueil",$this->generateUrl("back_index"));
        $breadcrumbs->addItem("Liste des Mots de passe");

        $new_key_pass = new KeyPass();
        $formAddKeyPass =$this->createForm(AddKeyPassType::class, $new_key_pass);
        $formAddKeyPass->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($formAddKeyPass->isSubmitted() && $formAddKeyPass->isValid())
        {
            if((strlen($new_key_pass->getPassword()>5 && $new_key_pass->getPassword()<25)) && $new_key_pass->getUsername()!== null){
                $keyPassManager->upsert($new_key_pass);

                return $this->redirectToRoute('back_key_pass_index');
            }

        }

        if ($this->isGranted('ROLE_BACK_KEYPASS_MANAGER')){
            $key_passes = $em->getRepository(KeyPass::class)->findBy(array('isActive'=> true));
        } else {
            $key_passes = $em->getRepository(KeyPass::class)->getListKeyPass($this->getUser());
        }


        $param_render = array(
            'keyPasses'=>$key_passes,
            'formAddKeyPass' => $formAddKeyPass->createView()
        );

        return $this->render('Back/KeyPass/list.html.twig', $param_render);
    }

    /**
     * @Route("/display/keypass/{id}", name="back_display_keyPass")
     */
    public function keypassById(KeyPass $keyPass, KeyPassInterface $keyPassManager): Response
    {
        $keyPass->setPassword($keyPassManager->displayPassword($keyPass));

        $param_render=array(
            'keyPass'=> $keyPass
        );

        return $this->render('Back/KeyPass/_ajax/view.html.twig', $param_render);
    }

    /**
     * @Route("/edit/keypass/{id}", name="back_edit_keyPass")
     */

    public function keypassEdit( Request $request, KeyPass $keyPass, KeyPassInterface $keyPassManager,
                                 Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->addItem("Accueil",$this->generateUrl("back_index"));
        $breadcrumbs->addItem("Liste des Mots de passe", $this->generateUrl("back_key_pass_index"));
        $breadcrumbs->addItem("Edition de Mot de passe");

        $keyPass->setPassword($keyPassManager->displayPassword($keyPass));
        $formEdit = $this->createForm(EditKeyPassType::class, $keyPass);
        $formEdit->handleRequest($request);

        if($formEdit->isSubmitted()&&$formEdit->isValid())
        {
            if((strlen($keyPass->getPassword()>5 && $keyPass->getPassword()<25))&&$keyPass->getUsername()!==null)
            {
                $keyPassManager->upsert($keyPass);
            }
            return $this->redirectToRoute('back_edit_keyPass', array('id'=> $keyPass->getId()));
        }


        //$keyPass->setPassword($keyPassManager->displayPassword($keyPass));

        $param_render=array(
            'formEditKeyPass'=> $formEdit->createView(),
            'keyPass'=> $keyPass
        );


        return $this->render('Back/KeyPass/edit.html.twig', $param_render);
    }

    /**
     * @Route("/toggle/keypass/{id}", name="back_keypass_toggle")
     */
    public function deleteKeyPass(KeyPass $keyPass)
    {
        $em = $this->getDoctrine()->getManager();
        $keyPass->setDeletedAt(($keyPass->getIsActive())?new \DateTime('now'):null);
        $keyPass->setIsActive(!$keyPass->getIsActive());
        $em->persist($keyPass);
        $em->flush();

        return $this->redirectToRoute('back_key_pass_index');
    }




//
//    /**
//     * @Route("/new", name="key_pass_new", methods={"GET","POST"})
//     */
//    public function new(Request $request): Response
//    {
//        $keyPass = new KeyPass();
//        $form = $this->createForm(KeyPassType::class, $keyPass);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->persist($keyPass);
//            $entityManager->flush();
//
//            return $this->redirectToRoute('key_pass_index');
//        }
//
//        return $this->render('key_pass/new.html.twig', [
//            'key_pass' => $keyPass,
//            'form' => $form->createView(),
//        ]);
//    }
//
//    /**
//     * @Route("/{id}", name="key_pass_show", methods={"GET"})
//     */
//    public function show(KeyPass $keyPass): Response
//    {
//        return $this->render('key_pass/show.html.twig', [
//            'key_pass' => $keyPass,
//        ]);
//    }
//
//    /**
//     * @Route("/{id}/edit", name="key_pass_edit", methods={"GET","POST"})
//     */
//    public function edit(Request $request, KeyPass $keyPass): Response
//    {
//        $form = $this->createForm(KeyPassType::class, $keyPass);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('key_pass_index');
//        }
//
//        return $this->render('key_pass/edit.html.twig', [
//            'key_pass' => $keyPass,
//            'form' => $form->createView(),
//        ]);
//    }
//
//    /**
//     * @Route("/{id}", name="key_pass_delete", methods={"POST"})
//     */
//    public function delete(Request $request, KeyPass $keyPass): Response
//    {
//        if ($this->isCsrfTokenValid('delete'.$keyPass->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($keyPass);
//            $entityManager->flush();
//        }
//
//        return $this->redirectToRoute('key_pass_index');
//    }
}
