<?php

namespace App\Form\Back\KeyPass;

use App\Entity\Security\Back\KeyPass;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddKeyPassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Nom d\'utilisateur',
                'required' => true
            ))
            ->add('password', PasswordType::class, array(
                'label' => 'Mot de passe',
                'required' => true
            ))
            ->add('url', UrlType::class, array(
                'label' => 'Url',
                'required' => false
            ))
            ->add('isPrivate', CheckboxType::class, array(
                'label' => 'MDP Privé',
                'required' => true
            ))
            ->add('description', TextareaType::class, array(
                'label' => "Description",
                'attr' => array(
                    'class' => 'ckeditor'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KeyPass::class,
        ]);
    }
}
