<?php

namespace App\Form\Back\KeyPass;

use App\Entity\Security\Back\BackUser;
use App\Entity\Security\Back\KeyPass;
use App\Form\Back\BackUser\BackUserType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditKeyPassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'Nom d\'utilisateur',
                'required' => true
            ))
            ->add('password', TextType::class, array(
                'label' => 'Mot de passe',
                'required' => true
            ))
            ->add('url', UrlType::class, array(
                'label' => 'Url',
                'required' => false
            ))
            ->add('isPrivate', CheckboxType::class, array(
                'label' => 'MDP Privé',
                'required' => true
            ))
            ->add('description', TextareaType::class, array(
                'label' => "Description",
                'attr' => array(
                    'class' => 'ckeditor'
                )
            ))
            ->add('shared_with', CollectionType::class, array(
                'entry_type'=> EntityType::class,
                'entry_options' => array(
                    'label'=> false,
                    'class' => BackUser::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->where('u.isActive = 1')
                            ->orderBy('u.firstName');
                    },
                    'choice_label' => 'firstName',
                    'expanded' => false,
                    'multiple' => false,
                    'attr'=>["class"=>"col-6"]
                ),
                'label' => 'Partagé avec',
                'allow_add' => true,
                'allow_delete'=> true,
                'prototype' => true

//                'by_reference' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KeyPass::class,
        ]);
    }
}
