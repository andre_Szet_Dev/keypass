var Encore = require('@symfony/webpack-encore');
Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    //.setPublicPath('/build')
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix(env.public_path_prefix+'/build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('back', './assets/back.js')
    .addEntry('back_security', './assets/back_security.js')
    .addEntry('app_security', './assets/app_security.js')
    .addEntry('app', './assets/app.js')
    .addEntry('menu_1', './assets/menu1.js')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    // support SASS https://symfony.com/doc/current/frontend/encore/css-preprocessors.html
    // don't forget to install sass-loader and node-sass
    // yarn add sass-loader@^7.0.1 node-sass --dev
    .enableSassLoader()
    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin

    .autoProvideVariables({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery',
        'window.jQuery': 'jquery',
    })


    .autoProvidejQuery()


    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')

    // .node({fs:'empty'})

    // CKEditor Install
    // .copyFiles([
    //     {from: './node_modules/ckeditor/', to: 'ckeditor/[path][name].[ext]', pattern: /\.(js|css)$/, includeSubdirectories: false},
    //     {from: './node_modules/ckeditor/adapters', to: 'ckeditor/adapters/[path][name].[ext]'},
    //     {from: './node_modules/ckeditor/lang', to: 'ckeditor/lang/[path][name].[ext]'},
    //     {from: './node_modules/ckeditor/plugins', to: 'ckeditor/plugins/[path][name].[ext]'},
    //     {from: './node_modules/ckeditor/skins', to: 'ckeditor/skins/[path][name].[ext]'}
    // ])
    // .addLoader({test: /\.json$/i, include: [path.resolve(__dirname, 'node_modules/ckeditor')], loader: 'raw-loader', type: 'javascript/auto'})

    // .addPlugin( new CKEditorWebpackPlugin( {
    //     // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
    //     // The main language that will be built into the main bundle.
    //     language: 'fr',
    //
    //     // Additional languages that will be emitted to the `outputDirectory`.
    //     // This option can be set to an array of language codes or `'all'` to build all found languages.
    //     // The bundle is optimized for one language when this option is omitted.
    //     //additionalLanguages: 'all',
    //     additionalLanguages: ['en','es','fr','de'],
    //
    //     // An optional directory for emitted translations. Relative to the webpack's output.
    //     // Defaults to `'translations'`.
    //     //outputDirectory: 'ckeditor5-translations',
    //
    //     // Whether the build process should fail if an error occurs.
    //     // Defaults to `false`.
    //     // strict: true,
    //
    //     // Whether to log all warnings to the console.
    //     // Defaults to `false`.
    //     //verbose: true
    // } ) )

    // Use raw-loader for CKEditor 5 SVG files.
    // .addRule( {
    //     test: /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/,
    //     loader: 'raw-loader'
    // } )

    // Configure other image loaders to exclude CKEditor 5 SVG files.
    // .configureLoaderRule( 'images', loader => {
    //     loader.exclude = /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/;
    // } )

    // Configure PostCSS loader.
    // .addLoader({
    //     test: /ckeditor5-[^/\\]+[/\\]theme[/\\].+\.css$/,
    //     loader: 'postcss-loader',
    //     options: styles.getPostCssConfig( {
    //         themeImporter: {
    //             themePath: require.resolve('@ckeditor/ckeditor5-theme-lark')
    //         }
    //     } )
    // } )
    .copyFiles({
        from: './assets/images',

        // optional target path, relative to the output dir
        to: 'images/[path][name].[ext]',

        // if versioning is enabled, add the file hash too
        //to: 'images/[path][name].[hash:8].[ext]',

        // only copy files matching this pattern
        //pattern: /\.(png|jpg|jpeg)$/
    })
;

// module.exports = Encore.getWebpackConfig();

var config = Encore.getWebpackConfig();
// config.node = { fs: 'empty' };
// const imagesLoader = config.module.rules.find(rule => rule.loader === 'images-loader');
// imagesLoader.exclude = /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/;
module.exports = config;
